import unittest
import tests
import mystirling
epsilon = 0.00001
#python -m unittest unitTests

class unitTests(unittest.TestCase):

# --- Fonction usuelle ---
	def assertEq(self, tab1, tab2):
		if(len(tab1) != len(tab2)):
			self.assertTrue(False, "Tableau de differente taille. got\n"+str(tab1))
		for i in range(len(tab1)):
			diff = tab2[i]-tab1[i]
			if(not( -epsilon<diff and diff<epsilon)):
				self.assertTrue(False, " expected\n"+str(tab2)+"\n got\n"+str(tab1))
		return True

# --- tests pour stirling ---
	def test_stirling1(self):
		self.assertEqual(mystirling.stirling(30, 2) , 536870911)

	def test_stirling2(self):
		self.assertEqual(mystirling.stirling(15, 7) , 408741333)

	def test_stirling3(self):
		self.assertEqual(mystirling.stirling(150, 150) , 1)

	def test_stirling4(self):
		self.assertEqual(mystirling.stirling(150, 1) , 1)

# --- test pour la fusion de classes ---
	def test_fusion1(self):
		classes = [0,0,0,2,3,4,5,6,0,3,0,0,7,5]
		tests.fusionzeros( classes )
		self.assertEq( classes, [2,3,4,5,6,3,7,5])

	def test_fusion2(self):
		classes1 = [0,0,0,2,3,4,5,6,0,3,0,0,7,5]
		classes2 = [1,2,3,4,5,6,7,8,9,8,7,6,5,4]
		tests.fusionzeros( classes1 , classes2 )
		self.assertEq( classes2, [10,5,6,7,8,17,18,4])

	def test_fusion3(self):
		classes = [1,2,3,0,5,6,7,8,0]
		tests.fusionzeros( classes)
		self.assertEq( classes, [1,2,3,5,6,7,8])

	def test_fusion4(self):
		classes1 = [1,2,3,0,5,6,7,8,0,0]
		classes2 = [8,8,8,8,8,8,8,8,6,6]
		tests.fusionzeros(classes1 , classes2)
		self.assertEq( classes2 , [8,8,8,16,8,8,20])

# --- tests pour test de collectionneur de coupon ---
	def test_couponeclasses(self):
		classes = tests.couponeclasses(5,100)
		self.assertEq(classes , [0.036288,0.163296,0.4191264,0.8083152,99.3812896])

	def test_couponoclasses1(self):
		classes = tests.couponoclasses([2,0,1], 5, nd=3)
		self.assertEq(classes , [1,0,0,0,0])

	def test_couponoclasses2(self):
		classes = tests.couponoclasses([2,0,1,0], 5, nd=3)
		self.assertEq(classes , [1,0,0,0,0])

	def test_couponoclasses3(self):
		classes = tests.couponoclasses([2,0,1,1,1,1,0,2,1], 5, nd=3)
		self.assertEq(classes , [1,0,1,0,0])

	def test_couponoclasses4(self):
		classes = tests.couponoclasses([2,0,1,1,1,1,0,2,1,1,1,1,0,0], 5, nd=3)
		self.assertEq(classes , [1,0,1,0,1])

	def test_couponoclassesFinal(self):
		classes = tests.couponoclasses([0,1,2,3,3,1,2,0,0,0,0,2,3,1,3,2,2,2,1,1,1,1,1,0,2,3,2] , 4, nd=4)
		self.assertEq(classes , [2,0,2,1])

# --- tests pour test de permutation ---
	def test_fpermut(self):
		tup = [1,5,6,3,4,2]
		obt = tests.fpermut(tup)
		self.assertEqual(obt, 19)

	def test_permutationoclasses1(self):
		classes = tests.permutationoclasses([1,2,3], k=3)#b=0,1,0
		self.assertEq( classes, [0,1,0,0,0,0])

	def test_permutationoclasses2(self):
		classes = tests.permutationoclasses([1,2,3,4,5,6,7,8,9,3,2,1,1,3,2], k=3)#pour 1,3,2 b=0,0,0
		print tests.fpermut([1,3,2])
		self.assertEq( classes, [1,3,0,0,0,1])

# --- tests pour test du poker ---
	def test_pokereclasses1(self):
		classes = tests.pokereclasses(5)
		self.assertEq( classes , [0.0001,0.0135,0.18,0.504,0.3024])

	def test_pokereclasses2(self):
		classes = tests.pokereclasses(5, population=10000)
		self.assertEq( classes , [1, 135, 1800, 5040, 3024])

	def test_pokeroclasses(self):
		classes = tests.pokeroclasses([1,2,3,4,5,9,9,8,8,1,6,6,6,6,6,6,6,6,6,6,9,8,7,6,6,3,2,1,6,6], main=5)
		self.assertEq( classes , [2,0,1,2,1])

# --- tests pour test de gap ---
	def test_gapeclasses(self):
		self.assertEq( tests.gapeclasses(0.0, 0.6, 5, 10), [6.0, 2.4, 0.96, 0.384, 0.256])

	def test_gapoclasses(self):
		classes = tests.gapoclasses([0.1,0.7,0.8,0.1,0.1,0.9,0.9,0.9,0.9,0.2,0.3,0.8,0.1], 0.0, 0.6, 6)
		self.assertEq(classes , [2,1,1,0,1,0])

	def test_gapeclassesDigit(self):
		classes = tests.gapeclasses(0,6, 5, 5, digit=True)
		self.assertEq(classes, [3.0, 1.2, 0.48, 0.192, 0.128])

	def test_gapoclassesDigit(self):
		classes = tests.gapoclasses([1,7,8,1,1,9,9,9,9,2,3,8,1], 0, 6, 6)
		self.assertEq(classes, [2,1,1,0,1,0])
