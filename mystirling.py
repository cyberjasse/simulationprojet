# -*- coding: utf-8 -*-

"""
Mon implémentation de la fonction de stirling de seconde espèce (second kind)
"""
stirlingTab = [[1],[1,1],[1,3,1],[1,7,6,1],[1,15,25,10,1],[1,31,90,65,15,1],[1,63,301,350,140,21,1],[1,127,966,1701,1050,266,28,1],[1,255,3025,7770,6951,2646,462,36,1],[1,511,9330,34105,42525,22827,5880,750,45,1]]#,[1,1023,28501,145750,246730,179487,63987,11880,1155,55,1],[1,2047,86526,611501,1379400,1323652,627396,159027,22275,1705,66,1],[1,4095,261625,2532530,7508501,9321312,5715424,1899612,359502,39325,2431,78,1],[1,8191,788970,10391745,40075035,63436373,49329280,20912320,5135130,752752,66066,3367,91,1],[1,16383,2375101,42355950,210766920,420693273,408741333,216627840,67128490,12662650,1479478,106470,4550,105,1]]

def stirling(k, r):
	"""
	Calcule le nombre de stirling de deuxième espece
	:param k: La longueur de séquence étudiée
	:param r: La main etudiée
	:return: Le nombre de stirling pour ces paramètres
	.. note: La description des paramètres explique le lien entre les paramètres du nombre de stirling et le test du poker
	.. note: Dans les documents trouvés sur internet, r c'est k et k c'est n
	"""
	if (k<0 or r<0):
		print "Error k<0 ou r<0"
		exit()
	elif (r>k):
		print "Error r>k"
		exit()
	#cas de base
	elif (k==r or r==1):
		return 1
	elif (r==0):
		return 0
	#cas enregistré
	elif (stored(k-1,r-1)):
		return stirlingTab[k-1][r-1]
	#cas general
	else:
		#print "cas non enregistré: k="+str(k)+" r="+str(r)
		store(k-1,r-1, stirling(k-1, r-1) + r*stirling(k-1,r) )
		return stirlingTab[k-1][r-1]

def stored(a,b):
	while(len(stirlingTab) <= a):
		stirlingTab.append([])
	while(len(stirlingTab[a]) <= b):
		stirlingTab[a].append(-1)
	if(stirlingTab[a][b] != -1):
		return True
	else:
		return False

def store(a,b, data):
	stirlingTab[a][b] = data
