\section{Étude du caractère pseudo-aléatoire des décimales de $\pi$}
Pour tester le caractère pseudo-aléatoire des 1.000.000 premières décimales de $\pi$, nous allons d'abord vérifier que si ces décimales répondent à une loi aléatoire, alors elles répondent à une loi aléatoire uniforme.
Premièrement, il faut s'assurer de la conformité de l'hypothèse suivante : ``Chaque digit apparaît le même nombre de fois dans la séquence''.
Ensuite, il faut vérifier si l'agencement des digits ne suit pas un certain \emph{pattern}, à cet effet, un test de collectionneur de coupons et un test de poker seront utilisés.
\subsection{Notions théoriques}
\subsubsection{Nombres de Stirling de seconde espèce}
\newcommand{\stir}[2]{{~#1~\brace ~#2~}}
Les formules des fréquences théoriques pour les tests suivants utilisent la formule de Stirling:
$$\text{Stirling(k,r)} = \stir{k}{r} = \stir{k-1}{r-1} + r\stir{k-1}{r}$$
Avec le cas de base
$$\stir{k}{1} = \stir{k}{k} = 1$$
\subsubsection{Test de poker}
Soit $k,d$ des entiers, avec $k \le d$ où $d$ est le nombre de classes (le nombre de digits différents dans notre cas), générons plusieurs k-tuples de nombres. Ces tuples sont appelés des ``mains''.
Pour chaque main, on compte le nombre de digits distincts dont elle dispose et ce nombre correspond à la classe à laquelle appartient cette main.
Il y aura donc $k$ classes.
Il se peut que les fréquence des premières classes soient trop petites pour un test de \chid.
Nous pouvons fusionner ces classes pour obtenir une classe plus conséquente mais il faudra alors ne pas oublier de fusionner les classes correspondantes dans les valeurs théoriques.
Mais avec le paramètre que nous avons choisi ($k = 5$), nous n'avons pas de fréquences trop petites.
Plus $k$ est grand, plus on risque d'obtenir des classes trop petites surtout dans les premières classes.

La probabilité qu'une main de taille $k$ généré avec une loi aléatoire uniforme se trouve dans la classe $r$ est :
$$P_{r} = \frac{\stir{k}{r}\prod_{i=0}^{r-1}(d-i)}{d^{k}}$$

\subsubsection{Test de collectionneur de coupons}
Soit $t$ une taille maximale de coupons.
Si on veut tester un générateur de nombres $\in [0,1[$ alors on définit des intervalles de tailles égales recouvrant [0,1[.
Mais dans notre cas des décimales de $\pi$, ces intervalles seront en fait les digits de 0 jusque 9. 

Le principe de ce test est de générer (parcourir) des nombres jusqu'à obtenir un coupon, c'est à dire une séquence où on a au moins un nombre pour chaque intervalle, ou jusqu'à obtenir $t$ nombres.
Si après avoir parcouru $t$ nombres on n'a toujours pas de coupon, alors on incrémente de 1 la fréquence de la dernière classe, c'est à dire la classe contenant les cas où on n'a pas de coupons après $t$ nombres.
Sinon on incrémente la fréquence de la $t^{ieme}$ classe.
Puis on recommence ce procédé.
Il y aura donc $t+1$ classes.
Remarquons que plus $t$ est grand, plus le risque d'avoir des classes trop petites dans les dernières classes est important.

Soit $d$ le nombre d'intervalles ou de valeurs possibles, la probabilité qu'un coupon soit de taille $r \le t$ est :
$$S_{r} = \frac{d!}{d^{r}}\stir{r-1}{d-1}$$
Et la probabilité de ne pas avoir de coupon après avoir parcouru $t$ nombres est :
$$1 - \stir{t-1}{d}\frac{d!}{d^{t-1}}$$

\subsection{Vérification de l'uniformité de la loi}
Si nous comptons le nombre d'occurences des chiffres parmi les 1.000.000 premières décimales de $\pi$, nous obtenons la figure \ref{occurence}

\begin{figure}[h!]\label{occurence}
\parbox[t]{6cm}{\null
  \centering
  \includegraphics[scale=0.25]{piDecimals.jpg}
  \captionsetup{justification=centering}
  \caption{Occurences des chiffres}%
}
\parbox[t]{9cm}{\null
\centering
  \begin{tabular}{c|c|c}
	Chiffre & eff. observés & eff.théoriques \\ 
	\hline
	0 & 99959 & 100000\\
	1 & 99758 & 100000\\
	2 & 100026 & 100000\\
	3 & 100229 & 100000\\
	4 & 100230 & 100000\\
	5 & 100359 & 100000\\
	6 & 99548 & 100000\\
	7 & 99800 & 100000\\
	8 & 99985 & 100000\\
	9 & 100106 & 100000\\
\end{tabular}
}
\end{figure}

On observe que le nombre d'occurences pour chaque chiffre est assez proche de la valeur attendue. Testons cette hypothèse avec un test de \chid.
Dans la table \ref{k_tablepi} nous donnons le résultat de ce test ainsi que la valeur-p et la valeur critique.

\begin{table}[H]
\centering
\begin{tabular}{c|c|c}
$K_{n}$ & $\chi_{\alpha=0.05,r-1=9}^{2}$ & valeur-p\\ 
\hline 
5,50908 & 16.91897760 & 0,787867\\
\end{tabular}
\caption{Résultat du test de \chid}
\label{k_tablepi}
\end{table}

Avec un risque de première espèce de 5\%, on peut garder l'hypothèse nulle. La loi régissant les décimales de $\pi$ est uniforme.
On va pouvoir utiliser les tests conçus pour étudier l'aléatoirité des générateurs suivant la loi uniforme.

\subsection{Observations pour le test de poker}
Nous avons effectué le test du poker sur ces décimales de $\pi$ avec des mains de taille 5 puis de taille 10.
Notons que pour les mains de taille 10, puisque les deux premières classes ont une fréquence de 0, nous les fusionnons avec la 3ème et nous faisons de même pour les classes de la loi théorique.


\textbf{Analyse d'un test de poker pour les décimales de $\pi$ : }
\begin{table}[h!]
\centering
\makebox[0pt][c]{\parbox{1\textwidth}{%
\begin{minipage}[c]{.45\linewidth}\centering
	\begin{tabular}{c|c|c}
	classe & eff. observés & eff. théoriques \\
	\hline  
	1 & 13 & 20 \\ 
	2 & 2644 & 2700 \\ 
	3 & 36172 & 36000 \\ 
	4 & 100670 & 100800 \\ 
	5 & 100800 & 60480 \\
	\end{tabular} 
	\captionsetup{justification=centering}
	\caption{Effectifs pour le test de poker avec des mains de 5}
\end{minipage}
\hfill
\begin{minipage}[c]{.45\linewidth}
	\begin{tabular}{c|c|c}
	classe & eff. observés & eff.théoriques \\ 
	\hline 
	1 & 0 & 0,0001 \\ 
	2 & 0 & 0,4599 \\ 
	3 & 61 & 67,176 \\ 
	4 & 1709 & 1718,892 \\  
	5 & 12957 & 12859,56 \\ 
	6 & 34407 & 34514,424 \\ 
	7 & 35623 & 35562,24 \\ 
	8 & 13531 & 13608 \\ 
	9 & 1647 & 1632,96 \\ 
	10 & 35 & 36,288 \\
	\end{tabular}
	\captionsetup{justification=centering}
	\caption{Effectifs pour le test de poker avec des mains de 10} 
\end{minipage}%
}}
\end{table}

\begin{figure}[htbp]
\begin{minipage}[c]{.45\linewidth}
\begin{center}
\includegraphics[scale=0.25]{Poker1.jpg}
\captionsetup{justification=centering}
\caption{Fréquences pour le test de poker avec des mains de 5}
\end{center}
\end{minipage}
\hfill
\begin{minipage}[c]{.45\linewidth}
\begin{center}
\includegraphics[scale=0.25]{Poker2.jpg}
\captionsetup{justification=centering}
\caption{Fréquences pour le test de poker avec des mains de 10}
\end{center}
\end{minipage}
\end{figure}

Ensuite, un test de l'hypothèse nulle, qui postule que les fréquences observées sont égales aux fréquences de la loi théorique, est effectué à l'aide d'un test de \chid.
Les résultats se trouvent dans la table \ref{k_poker} ci-dessous.

\begin{table}[H]
\centering
\begin{tabular}{c|c|c|c}
Taille des mains & $K_{n}$ & $\chi_{\alpha=0.05,\text{Taille main}-1}^{2}$ & valeur-p\\ 
\hline 
5 & 4,6082 & 9.48772904 & 0,33\\
10 & 2,21328 & 16.91897760 & 0,95\\
\end{tabular}
\caption{Résultat du test de \chid}
\label{k_poker}
\end{table}

En conclusion, avec un risque de première espèce de 5\%, nous pouvons garder l'hypothèse nulle.

\subsection{Observations pour le test de collectionneur de coupons}
Nous avons aussi effectué un test de collectionneur de coupons sur ces décimales de $\pi$ avec le paramètre $t$ fixé d'abord à 50 puis à 80.
Nous n'avons pas mis la valeur de la fréquence pour chaque classe car le tableau est trop long. Mais nous affichons tout de même les histogrammes obtenus et nous avons quand-même observé le tableau des fréquences observées et théoriques.

\begin{figure}[htbp]
\begin{minipage}[c]{.45\linewidth}
\begin{center}
\includegraphics[scale=0.25]{Coupons1.jpg}
\captionsetup{justification=centering}
\caption{Fréquences pour le test de coupons avec $t = 60$}
\end{center}
\end{minipage}
\hfill
\begin{minipage}[c]{.45\linewidth}
\begin{center}
\includegraphics[scale=0.25]{Coupons2.jpg}
\captionsetup{justification=centering}
\caption{Fréquences pour le test de coupons avec $t = 90$}
\end{center}
\end{minipage}
\end{figure}

Suite à l'observation des deux histogrammes nous pouvons dire que les fréquences ont l'air de se correspondre.
Les résultats d'un test de \chid pour comparer ces fréquences se trouvent dans la table \ref{k_coupons} ci-dessous.
Notons que nous ne prenons pas en compte les classes correspondant aux coupons de taille inférieure à 10 puisqu'ils vaudront toujours 0.
Donc le nombre de classes est $t+1-10$.

\begin{table}[H]
\centering
\begin{tabular}{c|c|c|c}
$t$ & $K_{n}$ & $\chi_{\alpha=0.05, t-10}^{2}$ & valeur-p\\ 
\hline 
60 & 48,5380463935 & 67.50480655 & 0,53\\
90 & 62.7974175692 & 113.14527014 & 0.92\\
\end{tabular}
\caption{Résultat du test de \chid}
\label{k_coupons}
\end{table}

Avec le risque de première espèce à 5\%, nous gardons l'hypothèse nulle.
