\section{Étude de nombres générés pseudo-aléatoirement}

Cette section a pour but la génération de nombres pseudo-aléatoires sur base des $1.000.000$ premières décimales de $\pi$. Ces nombres sont compris dans l'intervalle $[0,1[$, et afin d'établir une comparaison la plus correcte possible avec la génération pseudo-aléatoire de Python, ils comprennent 16 décimales comme c'est le cas pour les nombres pseudo-aléatoires obtenus à l'aide du générateur \emph{random.random()} de Python.

\subsection{Principe du générateur basé sur les décimales de $\pi$}

L'algorithme utilisé pour la génération de nombres pseudo-aléatoires se base sur une des rares valeurs non-déterministes qu'il est possible d'obtenir, le timestamp UNIX, c'est-à-dire le temps écoulé en secondes entre le moment présent et le 1$^{er}$ Janvier 1970.

La première étape consiste à récupérer ce timestamp et effectuer un modulo $1.000.000$ sur celui-ci, ce qui permet d'obtenir un nombre entre $0$ et $999.999$ compris. Ce dernier est alors utilisé comme indice dans le fichier contenant les décimales de $\pi$ . En effet à partir de l'indice trouvé, l'algorithme va alors lire 16x6 décimales de $\pi$, stockant ensuite ces 16 indices dans un tableau. 

La seconde étape consiste en la création du nombre pseudo-aléatoire en tant que tel. À l'aide des 16 indices récupérés précédemment, l'algorithme va aller extraire les décimales de $\pi$ correspondant à ces indices afin de créer un nombre constitué alors de 16 digits. Ce dernier va être ensuite précédé d'un "$0.$" afin d'obtenir un nombre appartenant à l'ensemble $[0,1[$.

La dernière étape de l'algorithme consiste en la génération du prochain nombre pseudo-aléatoire. Celle-ci s'effectue comme suit, les 16 indices contenus dans le tableau sont incrémentés de 1 (modulo $1.000.000$) afin d'obtenir 16 nouveaux digits pour pouvoir recommencer la seconde étape. Ce processus est répété exactement un million de fois (correspondant au nombre de décimales de $\pi$ disponibles). Après ce million d'itérations, afin d'éviter la présence de cycle dans notre génération de nombres pseudos-aléatoires, le timestamp UNIX est recalculé, et ainsi la base du tableau de 16 indices est renouvelée. 

\subsection{Notions théoriques}

\subsubsection{Test de gap}

Le test de Gap est basé sur le principe suivant, étant donné un ensemble de nombres et un intervalle comprenant une partie de ces nombres, il va parcourir toutes les valeurs de l'ensemble, repérer celles appartenant à l'intervalle et comptabiliser les distances (littéralement "gap") entre ses différentes occurences.
Par distance, on entend le nombre de nombres non repérés qui les séparent +1.
Dans le cas de l'analyse d'un générateur de réels compris dans [0,1] générés pseudo-aléatoirement, on repère les nombres appartenant à $[a,b]$ entièrement et pas qu'un seul chiffre comme dans le cas de l'analyse des décimales de $\pi$. Cette méthode peut-être expliquée de manière plus formelle, en effet : \\
Soient $t$ un entier ($t+1$ représentant le nombre de classes) et $[a,b]$ un intervalle, on parcourt progressivement la liste des nombres générés pseudo-aléatoirement et dès qu'un élément de la liste appartient à l'ensemble $[a,b]$ (nommons cet élément $i$), on compte alors la distance jusqu'à l'occurence suivante ($i+1$ , qui est appelé le gap du nombre) et ainsi de suite avec tous les nombres de l'intervalle, et ce pour toute la longueur de la liste. Si $i \le t$ alors on incrémente de 1 la fréquence de la classe $i+1$ (si indicé à partir de 1) sinon on incrémente la dernière classe, la classe $t+1$, représentant l'ensemble des gap de tailles plus grande que $t$. Ensuite à partir du dernier nombre parcouru, nous pouvons recommencer le comptage pour un nouveau gap. \\
Remarquons que si $t$ est trop grand, les dernières classes seront trop petites pour un test de \chid. \\

Pour toute classe $i \le t$, sa fréquence théorique est donnée par la formule suivante :
$$I_i = (1-p)^ip \text{ pour des classes indicées à partir de } 0 \text{ et } p = b-a$$ 

Et la fréquence théorique de la classe correspondant aux gaps supérieurs à $t$ vaut
$$I_{>t} = (1-p)^{t+1}$$

\subsubsection{Test de permutations}

Ce test ne peut pas se faire pour des nombres entiers car il suppose que le générateur génère des nombres réels distincts.
Soit $k$ la taille du tableau de classe. L'algorithme consiste au parcours de $k$ nombres générés pseudo-aléatoirement et numérotés de 0 à $k-1$.
La première opération est de trouver le minimum de cet ensemble de nombres, enregistrer son indice dans une liste $b$ et puis échanger la position de ce nombre avec le $k$ième nombre.
L'algorithme applique ensuite ce même prince mais avec les $k-1$ premiers nombres donc le minimum échangera sa place avec le $k-1$ième nombre. Et ainsi de suite jusqu'à ce que toute la liste soit parcourue, cette dernière sera alors triée de manière décroissante. 	 
Après toutes les itérations, la liste ${b_1, b_2, ..., b_k}$ contenant les indices des différentes permutations est complète et la formule suivante peut-être appliquée :
$$f = \sum_{i=1}^{k}b_{i}\frac{k!}{(k+1-i)!}$$

qui vaut, après une reformulation améliorant le temps de calcul:
$$f = \left(\left(...\left(\left( b_1\left(k-1\right) + b_2\right)\left(k-2\right) + b_3\right) ... \right)2 + b_{k-1}\right) + b_{k}$$

Et $f(b)$ donne l'indice de classe à laquelle appartient le k-tuple parcouru.
Et on recommence ainsi de suite en parcourant $k$ nombres à la fois. On a donc au total $k!$ classes.

En théorie, les fréquences obtenues doivent être identiques.

\subsection{Comparaison du générateur de $\pi$ avec le générateur Python}

Cette section consiste en l'analyse des deux générateurs utilisés afin d'obtenir des nombres normalisés pseudo-aléatoires. La différence avec la première partie, consistant en l'étude du caractère pseudo-aléatoire des décimales des Pi, est qu'ici le travail s'effectue sur l'intervalle $[0,1[$. Ce qui signifie pour les tests qu'un passage de la loi uniforme discrète à la loi uniforme continue est effectué. Concrètement, les classes contiendront maintenant des intervalles sur $[0,1[$.

Les deux tests effectués pour les générateurs sont, le test de gap et le test de permutations, tous deux suivi par une analyse des résultats via un test de $\chi^2$  afin de déterminer quel échantillon de nombre se rapproche le plus d'un échantillon suivant une loi normale continue. 

\subsubsection{Test de $\chi^2$}

Le principe du test de $\chi^2$ a déjà été expliqué précédemment dans la section \ref{chi2}. Il va ici être utilisé sur des intervalles de valeurs de taille $0.1$ pour une génération totale de $1.000.000$ de nombres. Ci-dessous sont indiqués les effectifs de chaque classe ainsi qu'une représentation en histogramme de ces derniers.

\begin{table}[H]
\centering
\begin{tabular}{c|c|c|c}
Classe & eff. observés (Python) & eff. observés (Pi) & eff. théoriques \\ 
\hline 
0 & 99408 & 99959 & 100000 \\ 
1 & 100078 & 99758 & 100000 \\ 
2 & 100280 & 100026 & 100000 \\ 
3 & 100173 & 100229 & 100000 \\ 
4 & 99635 & 100230 & 100000 \\ 
5 & 99973 & 100359 & 100000 \\ 
6 & 100305 & 99548 & 100000 \\ 
7 & 99946 & 99800 & 100000 \\ 
8 & 100187 & 99985 & 100000 \\ 
9 & 100015 & 100106 & 100000 \\ 
\end{tabular} 
\caption{Tableau des effectifs}
\end{table}

\begin{figure}[hbtp]
\centering
\includegraphics[scale=0.33]{Generators_comparison.jpg}
\caption{Fréquences des différentes classes}
\end{figure}

Nous pouvons remarquer que les effectifs observés sont assez proches des effectifs théoriques, ce qui montre bien que les nombres générés sont répartis équitablement dans les différents intervalles et que la loi de probabilité qui régit ces générateurs est uniforme. Pour afiner l'analyse, regardons les résultats du test de $\chi^2$ affichés dans la table \ref{k_table} (un $\alpha$ de 0.05 a été utilisé), ceux-ci montrent que le générateur Python est légèrement moins performant que celui se basant sur les décimales de $\pi$, tous deux étant bien inférieurs à $K_{\chi^2}$. Néanmoins cet exemple n'est pas représentatif des réelles performances des deux générateurs. En effet, il se peut qu'à une future exécution ce soit le générateur Python qui obtienne de meilleurs résultats étant donné que ces derniers sont très proches pour les deux générateurs. 

\begin{table}[H]
\centering
\begin{tabular}{c|c|c}
$K_{Python}$ & $K_{\pi}$ & $\chi_{\alpha=0.05,r-1=9}^{2}$ \\ 
\hline 
7.2997 & 5.5091 & 16.919 \\ 
\end{tabular}
\caption{Comparaison des valeurs obtenues} 
\label{k_table}
\end{table}

\subsubsection{Test de gap}

Afin d'analyser au mieux les résultats obtenus à l'aide de ce test nous allons premièrement effectuer un test sur l'intervalle $[0,0.5]$ avec $1.000.000$ nombres générés pseudo-aléatoirement et observer l'allure graphique des classes alors obtenues. Ensuite nous allons effectuer cinquante fois ce test et prendre les moyennes des test de $\chi^2$ étant donné que ces derniers peuvent varier assez significativement d'un test à l'autre. \\

\textbf{Analyse d'un test de gap pour les deux générateurs : }

\begin{table}[H]
\centering
\makebox[0pt][c]{\parbox{1.2\textwidth}{%
\begin{minipage}[c]{.45\linewidth}\centering
	\begin{tabular}{c|c|c}
	r & eff. observés & eff. théoriques \\
	\hline  
	1 & 249019 & 249714.50 \\ 
	2 & 125396 & 124857.25 \\ 
	3 & 62646 & 62428.63 \\ 
	4 & 30996 & 31214.31 \\ 
	5 & 15658 & 15607.16 \\ 
	6 & 7856 & 7803.58 \\  
	7 & 3893 & 3901.79 \\ 
	8 & 2031 & 1950.90 \\ 
	9 & 977 & 975.44 \\ 
	10 & 472 & 487.72 \\ 
	11 & 246 & 243.86 \\ 
	12 & 122 & 121.93 \\ 
	13 & 60 & 60.97 \\ 
	14 & 22 & 30.48 \\ 
	15 & 35 & 30.48 \\ 
	\end{tabular} 
	\caption{Effectifs pour le générateur Python}
\end{minipage}
\hfill
\begin{minipage}[c]{.45\linewidth}\centering
	\begin{tabular}{c|c|c}
	r & eff. observés & eff.théoriques \\ 
	\hline 
	1 & 249634 & 250100.50 \\ 
	2 & 125768 & 125050.25 \\ 
	3 & 62541 & 62525.13 \\ 
	4 & 31257 & 31262.56 \\  
	5 & 15476 & 15631.28 \\ 
	6 & 7748 & 7815.64 \\ 
	7 & 3924 & 3907.82 \\ 
	8 & 1876 & 1953.91 \\ 
	9 & 959 & 976.96 \\ 
	10 & 510 & 488.48 \\ 
	11 & 255 & 244.24 \\ 
	12 & 128 & 122.12 \\ 
	13 & 59 & 61.06 \\ 
	14 & 28 & 30.53 \\ 
	15 & 38 & 30.53 \\ 
	\end{tabular}
	\caption{Effectifs pour le générateur Pi} 
\end{minipage}%
}}
\end{table}

\begin{figure}[h!]
\begin{minipage}[c]{.35\linewidth}
\begin{center}
\includegraphics[scale=0.25]{Gap1.jpg}
\captionsetup{justification=centering}
\caption{Fréquences générateur Python}
\end{center}
\end{minipage}
\hfill
\begin{minipage}[c]{.35\linewidth}
\begin{center}
\includegraphics[scale=0.25]{Gap2.jpg}
\captionsetup{justification=centering}
\caption{Fréquences générateur Pi}
\end{center}
\end{minipage}
\end{figure}

\begin{table}[H]
\centering
\begin{tabular}{c|c|c}
$K_{Python}$ & $K_{\pi}$ & $\chi_{\alpha=0.05,r-1=14}^{2}$ \\ 
\hline 
13.9458 & 14.4388 & 23.685 \\ 
\end{tabular}
\caption{Comparaison des valeurs obtenues} 
\label{k_table2}
\end{table}

On peut remarquer selon ce test que les valeurs obtenues sont une fois encore assez proches des valeurs théoriques. On a également obtenu, comme on peut le voir à la table \ref{k_table2}, des résultats légèrement meilleurs pour le générateur Python. Cependant, un unique test ne permet pas de dégager une tendance globale.
Nous avons donc effectué le test sur 50 générations d'un million de nombre pour les deux générateurs.\\

\textbf{Résultats des 10 premiers tests de gap: }
\begin{table}[H]
\centering
\makebox[0pt][c]{\parbox{1\textwidth}{%
\begin{minipage}[c]{.45\linewidth}\centering
	\begin{tabular}{c|c|c}
	$K_n$ & valeur-p \\
	\hline
12.3158646746 & 0.580952531904 \\
11.3330340952 & 0.659698120045 \\
17.5701306212 & 0.227065790892 \\
9.7506454333 & 0.780177448252 \\
20.981994734 & 0.102094795026 \\
8.79404443183 & 0.84401342761 \\
12.3412450668 & 0.578918691994 \\
10.3142810262 & 0.738863911624 \\
3.22897324331 & 0.998594715975 \\
6.68005010285 & 0.946342957211 \\
	\end{tabular} 
	\captionsetup{justification=centering}
	\caption{Tests de gap pour le générateur Python}
\end{minipage}
\hfill
\begin{minipage}[c]{.45\linewidth}\centering
	\begin{tabular}{c|c|c}
	$K_n$ & valeur-p \\
	\hline
14.4431558513 & 0.417244174998 \\
14.4431558513 & 0.417244174998 \\
14.4431558513 & 0.417244174998 \\
14.4056377449 & 0.41994854362 \\
14.4693653151 & 0.415360030914 \\
14.4241926262 & 0.418610010674 \\
14.4370091963 & 0.417686651621 \\
14.3403904399 & 0.424671839518 \\
14.4511053623 & 0.416672258055 \\
14.478674786 & 0.41469180395 \\
	\end{tabular}
	\captionsetup{justification=centering}
	\caption{Tests de gap pour le générateur Pi} 
\end{minipage}%
}}
\end{table}

\textbf{Analyse des séries de tests de gap pour les deux générateurs : }

\begin{table}[H]
\centering
\begin{tabular}{c|c|c}
$K_{Python}$ & $K_{\pi}$ & $\chi_{\alpha=0.05,r-1=14}^{2}$ \\ 
\hline 
13.079500046353557 & 14.438560914849061 & 23.685 \\ 
\end{tabular}
\caption{Comparaison des $K_n$ moyens pour 50 itérations} 
\label{k_table3}
\end{table}

Nous remarquons que chaque valeurs des tests de \chid~sur les classes de notre générateur sont de 14 avec un partie décimale qui varie.
%TODO Pourquoi ?
Comparons maintenant la moyenne des valeurs $K_n$, on peut remarquer dans la table \ref{k_table3} un léger avantage à nouveau pour le générateur Python. Néanmoins, cet avantage ne peut être considéré comme acquis car il arrive parfois que la valeur de K du générateur $\pi$ soit inférieure à celle du générateur Python.
On a remarqué aussi que parmi les 50 résultats, pour $\alpha=0.05$, un résultat rejete l'hypothèse nulle pour le générateur de Python.

\subsubsection{Test de permutations}

Comme pour le test de gap, l'analyse se fera en deux parties, la première se basant sur une exécution du test et la deuxième sur un ensemble de cinquante itérations du test. Pour ces tests, la taille des tableaux des différentes classes (voir rappel théorique) a été fixé à 5! = 120. 

\begin{table}[H]
\centering
\begin{tabular}{c|c|c}
$K_{Python}$ & $K_{\pi}$ & $\chi_{\alpha=0.05,r-1=119}^{2}$ \\ 
\hline 
119.4016 & 110.3584 & 145.4607 \\ 
\end{tabular}
\caption{Comparaison des valeurs obtenues pour une itération} 
\end{table}

\begin{figure}[H]
\begin{minipage}[c]{.45\linewidth}
\begin{center}
\includegraphics[scale=0.25]{Permutation1.jpg}
\captionsetup{justification=centering}
\caption{Fréquences générateur Python}
\end{center}
\end{minipage}
\hfill
\begin{minipage}[c]{.45\linewidth}
\begin{center}
\includegraphics[scale=0.25]{Permutation2.jpg}
\captionsetup{justification=centering}
\caption{Fréquences générateur Pi}
\label{table1}
\end{center}
\end{minipage}
\end{figure}

\textbf{Résultats des 10 tests de permutation: }
\begin{table}[H]
\centering
\makebox[0pt][c]{\parbox{1\textwidth}{%
\begin{minipage}[c]{.45\linewidth}\centering
	\begin{tabular}{c|c|c}
	$K_n$ & valeur-p \\
	\hline
114.9268 & 0.588522169981 \\
112.588 & 0.647993560609 \\
124.3588 & 0.349989274661 \\
163.0396 & 0.0045902873665 \\
128.4784 & 0.260517443107 \\
126.5008 & 0.301755645244 \\
132.8632 & 0.18174272524 \\
112.1656 & 0.658489485102 \\
127.8436 & 0.273387320438 \\
88.6144 & 0.983144080798 \\
	\end{tabular} 
	\captionsetup{justification=centering}
	\caption{Tests de permutations pour le générateur Python}
\end{minipage}
\hfill
\begin{minipage}[c]{.45\linewidth}\centering
	\begin{tabular}{c|c|c}
	$K_n$ & valeur-p \\
	\hline
130.7836 & 0.216856340001 \\
153.8884 & 0.0172483072297 \\
127.7776 & 0.274745710564 \\
109.0984 & 0.731384670993 \\
98.0092 & 0.920157237233 \\
113.6116 & 0.622212847883 \\
130.81 & 0.216384919028 \\
120.0856 & 0.454874806407 \\
143.6284 & 0.0617239686866 \\
105.34 & 0.810064618561 \\
	\end{tabular}
	\captionsetup{justification=centering}
	\caption{Tests de permutations pour le générateur Pi} 
\end{minipage}%
}}
\end{table}

\begin{table}[H]
\centering
\begin{tabular}{c|c|c}
$K_{Python}$ & $K_{\pi}$ & $\chi_{\alpha=0.05,r-1=119}^{2}$ \\ 
\hline 
121.198936 & 124.25284 & 145.4607 \\ 
\end{tabular}
\caption{Comparaison des $K_n$ moyens pour les 10 itérations} 
\label{table2}
\end{table}

On peut remarquer que les résultats sont légèrement en faveur pour le générateur Random de python. Néanmoins cet avantage est léger et non-constant.
On remarque que parmis les 50 résultats, 2 résultats rejettent l'hypothèse nulle pour le générateur de Python et 5 rejettent l'hypthèse nulle pour le générateur à partir des décimales de $\pi$
Il aurait fallu effectuer un très grand nombre de tests mais notre générateur à partir des décimales de $\pi$ prend beaucoup de temps pour générer les nombres.
