# -*- coding: utf-8 -*-
import tests, random, time, math, os
import matplotlib.pyplot as plt
import numpy as np

alphas = [0.001,0.01,0.025,0.05,0.1,0.2,0.5]# pas en %
genIter = 1000000 #Nombres d'itérations dans le générateur
indicesTab = []

def loadpi(filename, n):
	"""
	Charge les n premières décimales de pi dans le fichier filename
	:param filename: Le nom de fichier
	:type filename: String
	:param n : Nombre de décimales à charger
	:type n : int
	:return: Tableau contenant les 1000000 décimales de pi
	:rtype: int[]
	.. note:: Le nom de notre fichier est pi6.txt
	"""
	
	f = open(filename,'r')
	tab = []
	i = 0
	c = f.read(3) # 3 . \n Ces 3 premiers caractères ne nous intéressent pas

	if n > 1000000:
		print "Le nombre de décimales de Pi dont nous disposons est de 1 million"
		print "Vous désirez " + str(n) + " décimales mais nous sommes obligés de réduire ce nombre à 1 million"
		n = 1000000 #Nous ne disposons que de 1 000 000 décimales de pi maximum

	while i != n:
		c = f.read(1)
		if c != '\n':
			tab.append(int(c))
			i = i+1

	f.close()
	return tab

def genRandPy():
	"""
	Renvoie un itérateur sur les nombres flottants pseudo-aléatoires de Python, ces 
	nombres appartiennent à [0,1[
	"""
	while True:
		yield random.random()

def genRandIntPi(piTab):
	"""
	Renvoie un itérateur sur des entiers générés pseudo-aléatoirement à l'aide des 1 000 000
	premières décimales de Pi.
	:param piTab: Le tableau contenant les 1 000 000 décimales de Pi 
	"""
	#Soit on itère pour la première fois, soit on a déjà itéré 1 million de fois, il faut alors
	#regénérer le timestamp
	global genIter
	global indicesTab

	while True:
		number = 0
		d = 10000000000000000
		if genIter == 1000000:
			index = (int) (time.time() % 1000000)
			genIter = 0
			getSixInTab(piTab, index)
			indicesTab = [getSixInTab(piTab, i) for i in xrange(index, index+91, 6)]
			for i in xrange(len(indicesTab)):
				d = d/10
				number = number + piTab[indicesTab[i]]*d
			yield number

		else:
			indicesTab = [(indicesTab[i] + 1) % 1000000 for i in xrange (16)]
			genIter = genIter + 1
			for i in xrange(len(indicesTab)):
				d = d/10
				number = number + piTab[indicesTab[i]]*d
			yield number

def getSixInTab(tab, index):
	"""
	Renvoie la concaténation de 6 chiffres contenus dans un tableau à partir de l'index donné
	:param tab: Le tableau contenant les chiffres à concaténer
	:param index: L'index du tableau à partir duquel on effectue la concaténation
	"""
	result = 0
	d = 1000000

	for j in xrange(index,index+6):
		d = d/10
		result = result + tab[j]*d

	return result

def genRandPi(piTab):
	"""
	Renvoie un itérateur sur des nombres flottants (précision max de 16 décimales) générés 
	pseudo-aléatoirement à l'aide des 1 000 000 premières décimales de Pi.
	:param piTab: Le tableau contenant les 1 000 000 décimales de Pi 
	"""
	while True :
		generator = genRandIntPi(piTab)
		yield generator.next() / (10. ** 16)

def histogram(observed, title, expected=None, observed_legend = 'observed', expected_legend = 'expected'):
	"""
	Dessine un histogramme sur base des données fournies
	:param observed: Le tableau des valeurs observées
	:param title: Titre de l'histogramme
	:param expected: Le tableau des valeurs théoriques
	"""

	width = 0.35 #Largeur des barres
	ind = np.arange(len(observed)) #Position des X pour les barres

	fig, ax = plt.subplots()

	rects1 = ax.bar(ind, observed, width, color='r')

	if expected != None:
		rects2 = ax.bar(ind + width, expected, width, color='y')

	ax.set_ylabel('Frequences')
	ax.set_xlabel('Classes')
	ax.set_title(title)

	if len(observed) < 42 : #Sinon devient illisible pour la lecture
		if expected != None :
			ax.set_xticks(ind + width)
		else :
			ax.set_xticks(ind + width/2)
		ax.set_xticklabels(range(len(observed)), rotation = 45)


	if expected != None:
		ax.legend((rects1[0], rects2[0]), (observed_legend, expected_legend))

	path = title + "1" + ".jpg"

	while os.path.isfile(path):
		path = title + str(int(path[-5])+1) + ".jpg"

	plt.savefig(path)


def histogramme(tab,maxc=10):
	"""
	:param tab: Un tableau contenant des nombres entre 0 et 9
	:type tab: int[]
	:return: Un tableau dont l'element à l'indice i est le nombre de fois ou i apparait dans tab
	:rtype: int[10]
	"""
	hist = []
	for i in range(maxc):
		hist.append(0)
	for d in tab:
		if isinstance(d, float):
			digit = (int)(d*10)
		else:
			digit = d
		hist[digit] = hist[digit]+1
	return hist

def mean(tab):
	"""
	Calcule la moyenne
	:type tab: int[]
	:return: La moyenne des nombres dans tab
	:rtype: float[]
	"""
	s = 0.0
	for i in tab:
		s = s+i
	return s/len(tab)

def gaptestloop(generator, loop, n):
	"""
	Effectue en boucle loop fois un test de gap sur l'intervalle [a,b] à size-1 degré de liberté sur des tableaux de taille n générés par generator
	"""
	intervals = [[0,0.5]]
	sizes = [15]
	histalphas = []
	kresult = []
	kmeans = []

	#Initialisation du tableau des résultats contenant K du test chi carré pour les différents intervalles
	kresult = [[] for i in xrange(len(intervals))]

	for i in range(len(alphas)):
		line = []
		for j in range(len(intervals)):
			line.append(0)
		histalphas.append(line)

	for i in range(loop):
		tab = [generator.next() for i in xrange(n)]
		for j in range(len(intervals)):
			pvalue = tests.gaptest(tab, intervals[j][0], intervals[j][1], sizes[j], scan=False)
			kresult[j].append(pvalue[0])
			for k in range(len(alphas)):
				if(alphas[k]<pvalue[1]): #Si la valeur-p est inférieure ou égale à alpha, on rejette l'hypothèse nulle
					histalphas[k][j] += 1

	kmeans = [mean(kresult[i]) for i in xrange(len(intervals))]	
	print "Moyenne des Kn : "
	print kmeans

	"""
	print " tests réussis pour"
	for i in range(len(intervals)):
		print "intervale ["+str(intervals[i][0])+","+str(intervals[i][1])+"] avec tableau de fréquences de taille "+str(sizes[i])
		for j in range(len(alphas)):
			print "alpha = "+str(alphas[j])+" : "+str(histalphas[j][i])+"/"+str(loop)+" tests réussis"
	"""

def permutationtestloop(generator, loop, n):
	"""
	Effectue en boucle loop fois un test de permutation sur des séquences de taille k sur des tableaux de taille n générés par generator
	"""
	histalphas = []
	ks = [5]

	#Tableau des résultats contenant K du test chi carré
	kresult = []

	for i in range(len(alphas)):
		line = []
		for j in range(len(ks)):
			line.append(0)
		histalphas.append(line)

	for i in range(loop):
		tab = [generator.next() for i in xrange(n)]
		for j in range(len(ks)):
			pvalue = tests.permutationtest(tab, ks[j], scan=False)
			kresult.append(pvalue[0])
			for l in range(len(alphas)):
				if(alphas[l]<pvalue[1]):
					histalphas[l][j] += 1
	"""
	print " tests réussis pour"
	for i in range(len(ks)):
		print "k = "+str(ks[i])
		for j in range(len(alphas)):
			print "alpha = "+str(alphas[j])+" : "+str(histalphas[j][i])+"/"+str(loop)+" tests réussis."
	"""

	kmeans = mean(kresult)
	print "Moyenne des Kn : "
	print kmeans


def random_classes (generator, n):
	"""
	Génère 10 classes remplies à l'aide de n nombres pseudo-aléatoires ([0,0.1],[0.1,0.2],...,[0.9,1[)
	"""
	tab = [generator.next() for i in xrange(n)]
	classes = [0 for i in xrange(10)]
	for i in xrange(n):
		classes[int(math.floor(tab[i]*10))] += 1

	for i in xrange(10):
		print "Classe " + str(i) + " : " + str(classes[i])

	chiresult = tests.myChiTest(classes, testname="Classes random" , scanhist=False)
	return classes

if __name__ == "__main__":
	#Chargement des décimales de Pi + création de notre tableau de n nombres aléatoires
	n = 1000000 #Nombre de décimales à charger
	loop = 50 #Nombre d'itérations à faire pour les tests sur un générateur random
	piTab = loadpi('pi6.txt', n)
	pyGen = genRandPy()
	piGen = genRandPi(piTab)
	print "Lecture finie\n"

	print " |||||| Tests sur les décimales de Pi ||||||"
	print " ------------------------------------------- "

	#Tests de chi2 pour répartition des n décimale de pi
	classes = histogramme(piTab)
	print classes
	histogram(classes, "Histogramme des decimales de Pi")
	tests.myChiTest(classes, scanhist = False)

	#test du poker
	tests.pokertest(piTab, main=5)
	tests.pokertest(piTab, main=10)

	#test de coupons
	tests.coupontest(piTab, 51)
	tests.coupontest(piTab, 81)
	
	print " |||||| Comparaison entre le générateur Python et le générateur Pi ||||||"
	print " ------------------------------------------------------------------------- "

	#Tests de chi^2, répartition des n nombres pseudos-aléatoire en 10 classes
	print "Test de chi^2 pour les deux générateurs"
	print "Classes de pyGen"
	py_classes = random_classes(pyGen, 1000000) #Nombres créés via le générateur Python
	print "Classes de piGen"
	pi_classes = random_classes(piGen, 1000000) #Nombres créés via le générateur Pi
	#Dessin de l'histogramme correspondant
	histogram(py_classes, "Generators comparison", pi_classes, observed_legend="Python generator", expected_legend="Pi generator")

	#Test de gap
	tabPy = [pyGen.next() for i in xrange(1000000)]
	tabPi = [piGen.next() for i in xrange(1000000)]
	print "  ## Py generator ##"
	gaptestloop(pyGen, loop, 1000000)
	print "  ## Pi generator ##"
	gaptestloop(piGen, loop, 1000000)

	#Test de permutations
	tabPy = [pyGen.next() for i in xrange(1000000)]
	tabPi = [piGen.next() for i in xrange(1000000)]
	print "  ## Py generator ##"
	permutationtestloop(pyGen, loop, n)
	print "  ## Pi generator ##"
	permutationtestloop(piGen, loop, n)
