# -*- coding: utf-8 -*-

"""
Module qui contient des implémentations de tests vus en cours.
Pour générer la doc (pydoc -w nomFichier), attention, ne pas mettre le ".py" dans nomFichier
"""
from scipy.stats import chisquare
from numpy import cov
from math import factorial
from mystirling import stirling
from pi6 import histogram

def myChiTest(observed,expected=None,testname="", scanhist=True):
	"""
	Cette fonction est en prévision d'un formatage de texte ou une génération de graphique automatisée
	Affiche juste les résultats d'un test de Chi carre 
	:param observed: Les fréquences observees pour chaque catégorie
	:param expected: Les fréquences theoriques pour chaque catégorie
	:param testname: Le nom du test à afficher
	"""
	print testname+" o= "+str(observed)
	if expected != None:
		print testname+" e= "+str(expected)
		if(len(observed) != len(expected)):
			print "Grosse erreur: taille des classes observées et théoriques fournies en paramètre du test de chi2 sont différentes."
	if(fusionzeros(observed,expected)):
		print testname+"  !!Attention!! Les classes ont changés pour éviter des fréquences nulles"
	chiresult = chisquare(observed, f_exp=expected)
	pvalue = chiresult[1] #la probabilité d'obtenir le même tableau de fréquence ou une meilleure du test si l'hypothèse nulle était vraie.
	print testname+": Kn= "+str(chiresult[0])+" et p-value= "+str(pvalue)
	if scanhist:
		histogram(observed, testname, expected)
	return chiresult

def fusionzeros(tab1, tab2=None):
	"""
	fusionne les classes voisines a une classe valant 0 pour faire en sorte qu'il n'y ait plus de zeros dans les classes
	Il faut quand meme verifier si on regroupe bien des petites classes
	:return: True si il y a eu une fusion
	"""
	if(tab2!=None and (len(tab1)!=len(tab2))):
		print "tab1 et tab2 pas de la meme taille"
		return
	fusion = False
	i = 0
	while(i < len(tab1)-1):
		if(tab1[i]==0 or (tab2!=None and tab2[i]==0)):
			fusion = True
			tab1[i] += tab1.pop(i+1)
			if(tab2 != None):
				tab2[i] += tab2.pop(i+1)
		else:
			i += 1
	#manque a fusionner le dernier si il vaut zero
	dernier = len(tab1)-1
	if(tab1[dernier]==0 or (tab2!=None and tab2[dernier]==0)):
		fusion = True
		tab1[dernier-1] += tab1.pop(dernier)
		if(tab2 != None):
				tab2[dernier-1] += tab2.pop(dernier)
	return fusion

def isin(number,a,b):
	"""
	Teste si number in [a,b]
	"""
	return a<=number and number<=b

def gapoclasses(tab, a, b, maxsize):
	"""
	Classes observée pour test du Gap
	:param tab: L'ensemble des nombres [0,1[ générés à tester
	:param a et b: bornes de l'intervalle des nombres à marquer. a<b
	:param maxsize: la taille du tableau de fréquence
	"""
	if(a>=b):
		print "Il faut que a<b pour le test de gap"
		quit()
	LIMIT = len(tab)
	classes = []
	for i in range(maxsize):
		classes.append(0)
	#on avance jusqu'au premier nombre marqué
	i = 0
	while(i < LIMIT):
		if(isin(tab[i],a,b)):
			break #i est maintenant l'indice du premier nombre marqué
		else:
			i += 1
	#on maintenant remplir les classes observées
	size = 1 #taille de sequence
	i += 1
	while(i < LIMIT):
		if(isin(tab[i],a,b)):
			#+1 dans la classe correspondante
			if(size > maxsize):
				size = maxsize
			classes[size-1] += 1
			size = 0
		i += 1
		size += 1
	return classes

def gapeclasses(a, b, size, numgap, digit=False):
	"""
	Classes théorique de taille size pour test de gap avec intervalle [a,b] et a<b
	:param numgap: le nombre de gap compté
	:param digit: à mettre sur True si il s'agit de nombre entre 0 et 9
	"""
	theoric = []
	p = b-a
	if digit:
		p = p/10.0
	ump = 1-p
	expo = 0
	for i in range(size-1):
		theoric.append( numgap*(ump**expo) * p )
		expo += 1
	theoric.append( numgap*(ump**expo))
	return theoric

def gaptest(tab, a, b, size, scan=True, digit=False):
	"""
	Effectue un test de gap
	:param tab: les nombres générés
	:param a et b: les bornes de l'intervalle
	:param size: la tailles des tableaux de fréquences
	:param digit: à mettre sur True si tab contient des digits
	"""
	#print " #### Test de gap ####"
	classes = gapoclasses(tab, a, b, size)
	numgap = sum(classes)
	theoric = gapeclasses(a, b, len(classes), numgap, digit)
	"""
	print "Classes observées : "
	for freq in enumerate(classes):
		print freq[1]
	print "Classes théoriques : "
	for freq in enumerate(theoric):
		print freq[1]
	"""	
	return myChiTest(classes, expected=theoric, testname="Gap", scanhist=scan)

def pokereclasses(k, population=1, d=10):
	"""
	Obtient les fréquences théoriques pour un test du poker
	:param k: La longueur des séquences étudiées
	:param population: Le nombre de mains étudiées (donc N/main ou main est le paramètre de pokertest)
	:param d: Le nombre de digits possibles (10 certainement)
	"""
	classes = []
	for i in range(0,k):
		r = i+1
		factor = 1
		for j in range(r):#compute d(d-1)...(d-r+1)
			factor *= d-j
		pr = stirling(k,r) * factor
		pr = pr*population
		pr = pr/(float)(d**k)
		classes.append(pr)
	return classes

def pokeroclasses(tab, main):
	"""
	Donnes les fréquences observées du test de poker à partir de tab un tableau de digit
	"""
	LIMIT = len(tab)
	if( LIMIT%main != 0):
		print "Erreur pokertest: Il faut que main divise len(tab)"
		exit()
	classes = []
	for k in range(main):
		classes.append(0)
	i = 0
	while(i<LIMIT):
		m = []#la main actuelle
		for j in range(main):
			m.append(tab[i+j])
		n = len(set(m))#le nombre de digit different dans m. en fait compte la taille d'une liste de m mais sans doublon
		classes[n-1] += 1
		i = i+main
	#maintenant on a les classes observees
	return classes

def pokertest(tab,main=10):
	"""
	Effectue un test de la main de poker sur une liste de digits.
	:param tab: L'ensemble des nombres générés
	:param main: La taille d'une main. len(tab) doit être divisible par main
	"""
	print " #### Test du poker ####"
	classes = pokeroclasses(tab, main)
	theoric = pokereclasses(main, len(tab)/main)
	return myChiTest(classes, expected=theoric, testname="Poker")

def fpermut(tup, modemin=True):
	"""
	Calcule l'indice de la permutation correspondant au tuple tup pour un test de permutation
	:param modemin: True pour une permutation sur les minimums, False pour une permutation sur les maximums
	:return: f
	"""
	k = len(tup)
	#build b
	b = []
	for j in range(k):
		#find the minimum
		minimum = tup[0]
		minimumindex = 0
		kmj = k-j
		for m in range(kmj):
			if ((modemin and tup[m]<minimum) or ((not modemin) and tup[m]>minimum)):
				minimum = tup[m]
				minimumindex = m
		kmj = kmj-1
		#permut minimum and tup[k-j-1]
		tup[minimumindex] = tup[kmj]
		tup[kmj] = minimum
		# and finally
		b.append(minimumindex)
	#compute f
	f = 0
	for j in range(k):
		f *= k-j
		f += b[j]
	return f

def permutationoclasses(tab, k=5, mode='min'):
	"""
	Donne les classes observées pour un test de permutation sur tab, une liste de nombre entre 0 et 1
	"""
	if( mode!='min' and mode!='max'):
		print "Erreur permutationtest: mode doit valoir 'min' ou 'max'"
		exit()
	if( mode=='max'):
		modemin = False
	else:
		modemin = True
	LIMIT = len(tab)
	kfact = factorial(k)
	if( LIMIT%k != 0):
		print "Erreur permutationtest: il faut que k divise len(tab)"
		exit()
	classes = []
	for i in range(kfact):
		classes.append(0)
	b = []
	for i in range(k):
		b.append(0)
	tup = []
	for i in range(k):
		tup.append(0)
	i = 0
	while(i<LIMIT):
		#copy the tuple
		for j in range(k):
			tup[j] = tab[i+j]
		#compute f
		f = fpermut(tup, modemin)
		#now we can add 1 to classes[f-1]
		classes[f] += 1
		i = i+k
	#maintenant on a les classes
	return classes

def permutationtest(tab, k=5, scan=True, mode='min'):
	"""
	Effectue un test de permutation. Il est conseillé que les nombres contenus dans tab soient des nombres flottants.
	:param tab: L'ensemble des nombre generes
	:param k: La taille des tuples. len(tab) doit être divisible par k
	:param mode: 'min' pour une permutation sur les minimums, 'max' pour une permutation sur les maximums.
	.. note: Quel que soit le mode, le résultat du tests de chi carre sera exactement le même.
	"""
	#print " #### Test de permutation ####"
	classes = permutationoclasses(tab, k, mode)
	return myChiTest(classes, testname="Permutation", scanhist=scan)

def couponoclasses(tab, deg, nd=10):
	"""
	Donne les classes observées pour un test de collectionneur de coupons sur tab, une liste de digit
	:param deg: La taille du tableau de fréquence
	:param nd: le nombre de digit disponibles à partir du digit 0. Option utilisé que pour les tests unitaires
	"""
	d = nd
	LIMIT = len(tab)
	classes = []
	for i in range(deg):
		classes.append(0)
	okdigit = []
	for i in range(nd):
		okdigit.append(False)
	i = 0
	while(i < LIMIT):
		for j in range(nd):
			okdigit[j] = False
		seqsize = 0
		allis = False
		while((not allis) and i<LIMIT and seqsize<deg-2+nd):#on va parcourir la séquence jusqu'à ce que tous les digits soient présents dedans
			okdigit[tab[i]] = True
			allis = True
			for boolean in okdigit:
				if(not boolean):
					allis = False
					break
			i += 1
			seqsize += 1
		#maintenant qu'on a la taille de la séquence, on met +1 dans la taille de sa classe.
		#si on a toujours pas de coupons après deg-1 nombres parcouru, on met +1 à la dernière classe
		if(allis):
			classes[seqsize-nd] += 1
		elif(seqsize == deg-2+nd):
			classes[deg-1] += 1
	return classes

def couponeclasses(deg, seqcount):
	"""
	Donne le tableau de taille deg de fréquences théoriques pour un test de coupons
	:param deg: La taille du tableau à retourner
	:param seqcount: Le nombre de séquences comptées
	"""
	d = 10
	factod = factorial(d)
	theorique = []
	r = 0
	for i in range(deg-1):
		r = i+10
		theorique.append(seqcount * factod*stirling(r-1,d-1)/(float)(d**r) )
	t = r
	theorique.append(seqcount * (1- stirling(t-1,d) * factod/(float)(d**(t-1))))
	return theorique
	
def coupontest(tab, deg=100):
	"""
	Effectue un test de collectionneur de coupons sur une liste de digits
	:param deg: La taille du tableau de fréquence: Après deg+10 nombre parcouru on abandonne le coupon en cours
	"""
	print " #### Test du collectionneur de coupons ####"
	classes = couponoclasses(tab, deg)
	theorique = couponeclasses(deg, sum(classes))
	return myChiTest(classes, expected=theorique, testname="Coupons")
